//@ts-check

class A {
  constructor(value) {
    this.value = value;
  }
}

const aCollection = [new A(1), new A(2), new A(3)];

A.prototype.value = 10;
A.prototype.value1 = 20;

console.log(aCollection.map((obj) => obj.value)); // ?

console.log(aCollection.map((obj) => obj.value1)); // ?