### Basic
- Inheritance in JavaScript
- Based on the following, what is the prototype chain for a?
```js
var a = ['str1', 'str2', 'str2'];
```
- What is a closure? Example please.
- [Task-5, what is the result for obj1.prop2?](./task-5.js)
- [Task-2?](./task-2.js)
- [Task-4?](./task-4.js)
- Which compilation methods do you know?

### Promise
- [Task-1?](./task-1.js)
- Run array with promises in parallel
- Run array with promises in series

### TS
- What is a type definition file?
- Difference between `interface & type Alias`
- Which helper types do you know?
- May I retrieve return type of function in TS?
- [Task-3, combine the two interfaces into a new](./task-3.ts) interface
- Type casting, how use type casting in TS?

### Common
- Test frameworks?
- `Webpack`, other packagers?
- Npm?
- Open source projects?