interface I1 {
	prop1: string;
}

interface I2 {
	prop2: string;
}

// 1
// type I3 = I1 extends I2;

// 2
// type I3 = I1 implements I2;

// 3
// type I3 = I1 & I2;

// 4
// type I3 = I1: I2;
