console.log(func2()); // ?
console.log(func1()); // ?

const func1 = () => 'func1';

function func2 () {
	return 'func2';
}