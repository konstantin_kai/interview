//@ts-check

const getPromise1 = () => Promise.resolve(1);
const getPromise2 = () => Promise.resolve(2);
const getPromise3 = () => Promise.reject(new Error());

getPromise1().then(getPromise2); // ?

getPromise1().then((res) => getPromise2()); // ?

getPromise3(); // ?

getPromise3().catch(getPromise1); // ?

getPromise2()
  .then((res) => {
    return getPromise3()
      .catch(() => res * 2)
      .then((res) => res * 2);
  })
  .then((res1) => getPromise1()
    .then((res2) => [res1, res2])); // ?

