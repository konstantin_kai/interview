var obj1 = { prop1: 1 };
var obj2 = obj1;

obj1.prop2 = obj1 = { prop1: 2 };

console.log(obj1.prop2); // ?

console.log(obj2); // ?